//
//  SecondViewController.m
//  Impact Hub Mobile app
//
//  Created by Sajjad Kamal on 2014-04-22.
//  Copyright (c) 2014 Innovio Labs. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
