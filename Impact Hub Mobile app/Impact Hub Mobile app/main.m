//
//  main.m
//  Impact Hub Mobile app
//
//  Created by Sajjad Kamal on 2014-04-22.
//  Copyright (c) 2014 Innovio Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
